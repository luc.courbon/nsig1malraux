Comme si je n'existais pas
Elle est passée à côté de moi
Sans un regard, reine de Saba
J'ai dit Aïcha, prend, tout est pour toi
Voici les perles, les bijoux
Aussi l'or autour de ton cou
Les fruits bien mûrs au goût de miel
Ma vie, Aïcha, si tu m'aimes
J'irai où ton souffle nous mènent
Dans les pays d'ivoire et d'ébène
J'effacerai tes larmes, tes peines
Rien n'est trop beau pour une si belle, ooh
Aïcha, Aïcha, écoute-moi
Aïcha, Aïcha, t'en vas pas
Aïcha, Aïcha, regarde-moi, oh-oh
Aïcha, Aïcha, réponds-moi
Je dirai les mots, les poèmes
Je jouerai les musiques du ciel
Je prendrai les rayons du soleil
Pour éclairer tes yeux de reine, ooh
Aïcha, Aïcha, écoute-moi, oh
Aïcha, Aïcha, t'en vas pas
Elle a dit "garde tes trésors
Moi, je vaux mieux que tout ça
Des barreaux sont des barreaux, même en or
Je veux les mêmes droits que toi
Et du respect pour chaque jour
Moi, je ne veux que de l'amour"
نبغيك عيشة ونموت عليك
هذه صية حياتي وحبي
إنت عمري وإنت حياتي
تمنيت نعيش معاك غير إنت
Aïcha, Aïcha, écoute-moi
عايشة، عايشة أنا نبغيك
Aïcha, Aïcha, t'en vas pas
عايشة، عايشة ونموت عليك
Aïcha, Aïcha, réponds-moi
Aïcha, Aïcha
Aïcha, Aïcha
Aïcha, Aïcha
