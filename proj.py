
# IMPORTATION DES LIBRAIRIES
import turtle
import math
from random import *


# REGLAGE VITESSE
turtle.speed(100)


# FONCTION QUI FAIT LE TRACAGE DE LA ROUTE
def route():
    turtle.width(3)
    turtle.forward(640)


# ---------------------------------------FONCTIONS DES COMPOSANTS DE L'IMMEUBLE -----------------------------------------------------




# FONCTION QUI FAIT LES FENETRES
def window(n):
    if n == 1:
        turtle.fillcolor("white")
        turtle.pencolor("black")
        turtle.begin_fill()
        turtle.forward(30)
        turtle.left(90)
        turtle.forward(30)
        turtle.left(90)
        turtle.forward(30)
        turtle.left(90)
        turtle.forward(30)
        turtle.left(90)
        turtle.end_fill()
    if n == 2:
        turtle.fillcolor("white")
        turtle.pencolor("black")
        turtle.begin_fill()
        turtle.forward(15)
        turtle.forward(30)
        turtle.left(90)
        turtle.forward(50)
        turtle.left(90)
        turtle.forward(30)
        turtle.left(90)
        turtle.forward(50)
        turtle.left(90)
        turtle.end_fill()
        for i in range(7):
            turtle.width(2)
            turtle.forward(5)
            for i in range(2):
                turtle.left(90)
                turtle.forward(20)
                turtle.left(90)
                turtle.forward(10)
            turtle.width(3)




# FONCTION QUI PROPOSE DEUX TYPES DE PORTES
def door(types):
    turtle.colormode(255)
    turtle.color(randint(0, 255), randint(0, 255), randint(0, 255)) # RANDOM COULEURS
    turtle.pencolor("black")
    if types == 1:
        turtle.begin_fill()
        turtle.forward(30)
        turtle.left(90)
        turtle.forward(50)
        turtle.left(90)
        turtle.forward(30)
        turtle.left(90)
        turtle.forward(50)
        turtle.end_fill()
    if types == 2:
        turtle.begin_fill()
        turtle.forward(30)
        turtle.left(90)
        turtle.forward(35)
        turtle.circle(15, 180)
        turtle.forward(35)
        turtle.end_fill()



# FONCTION ROOFTOP
def roof(formes):
    if formes == 1:
        turtle.fillcolor("black")
        turtle.begin_fill()
        for i in range(3):
            turtle.forward(140)
            turtle.left(120)
        turtle.end_fill()
    else:
        pass # Je n'ai pas reussi a implementer le deuxieme type de plafond

"""
PROGRAMME DE TOIT BETA EN FONCTION DUNE HAUTER AVEC DE LA TRIGO BASIQUE
def toit1():
    x = 10
    turtle.penup()
    turtle.goto(20,10)
    turtle.fillcolor('blue')
    turtle.begin_fill()
    turtle.goto(20,x)
    turtle.penup
    turtle.setheading(180)
    turtle.forward(f)
    turtle.right(90)
    turtle.penup()
    turtle.forward(h)
    turtle.pendown()
    z = (math.atan(f/h)*180/3.141592653)
    turtle.right(180-z)
    turtle.setpos(1,1)
    turtle.setheading(180)
    turtle.forward(f)
    turtle.forward(f)
    turtle.setheading(180)
    turtle.setpos(-f,h)
    turtle.setpos(1,1)

    """



# FONCTION QUI DEFINI LA POSITION DE BASE
def base_pos():
    turtle.up()
    turtle.goto(-320, -270)
    turtle.down()


# FONCTION QUI RECONSTITUE LES COMPOSANTS DE LIMEUBLE ET QUI CONSTRUIT LES ETAGES
def immeuble(z):
    turtle.colormode(255)
    c = randint(1, 255), randint(1, 255), randint(1, 255)
    turtle.color(c)
    turtle.pencolor("black")
    if z <= 5:
        for i in range(1, z + 1, 1):
            turtle.fillcolor(c)
            turtle.begin_fill()
            for v in range(2):
                turtle.forward(140)
                turtle.left(90)
                turtle.forward(60)
                turtle.left(90)
            turtle.end_fill()
            turtle.left(90)
            turtle.up()
            turtle.forward(60)
            turtle.down()
            turtle.right(180)
            turtle.forward(60)
            turtle.left(90)

            if i > 1:
                for v in range(3):
                    g = randint(1, 2)
                    if g == 1:
                        turtle.up()
                        turtle.forward(15)
                        turtle.left(90)
                        turtle.forward(15)
                        turtle.right(90)
                        turtle.down()
                    window(g)
                    if g == 1:
                        turtle.right(90)
                        turtle.up()
                        turtle.forward(15)
                        turtle.left(90)
                        turtle.forward(25)
                        turtle.down()
                    if g == 2:
                        turtle.backward(10)
                        turtle.right(turtle.heading())

                turtle.up()
                turtle.left(180)
                turtle.forward(120)
                turtle.right(90)
                turtle.forward(60)
                turtle.right(90)
                turtle.down()
            else:
                turtle.left(90)
                turtle.forward(60)
                turtle.right(90)

        roof(randint(1, 2))
        turtle.right(90)
        turtle.forward(60 * z)
        turtle.left(90)









def rez(m):
    if m == 1:
        turtle.forward(13)
        door(randint(1, 2))
        turtle.left(90)
        turtle.forward(72)
        turtle.left(90)
        turtle.up()
        turtle.forward(15)
        turtle.down()
        window(1)
        turtle.right(90)
        turtle.up()
        turtle.forward(12)
        turtle.down()
        window(1)
    if m == 2:
        turtle.forward(10)
        turtle.up()
        turtle.left(90)
        turtle.forward(15)
        turtle.down()
        turtle.right(90)
        window(1)
        turtle.right(90)
        turtle.up()
        turtle.forward(15)
        turtle.down()
        turtle.left(90)
        turtle.forward(44)
        door(randint(1, 2))
        turtle.left(90)
        turtle.forward(74)
        turtle.left(90)
        turtle.up()
        turtle.forward(15)
        turtle.down()
        window(1)





# FONCTION FINALE QUI LANCE LE TOUT
def finalfct():
    base_pos()
    route()
    base_pos()
    for i in range(4):
        immeuble(randint(1, 5))
        rez(randint(1, 2))
        turtle.up()
        turtle.goto(180 * i - 100, -200)
        turtle.right(turtle.heading())
        turtle.down()

finalfct()

